# frozen_string_literal: true

def fibs(n)
  return [1] if n == 1

  results = [1, 1]
  counter = 2
  while counter < n
    results << results[counter - 1] + results[counter - 2]
    counter += 1
  end
  results
end

def fibs_rec(n)
  return [1] if n == 1
  return [1, 1] if n == 2

  fibs_rec(n - 1) << fibs_rec(n - 1)[-1] + fibs_rec(n - 2)[-1]
end

puts fibs(5).to_s
puts fibs_rec(5).to_s
