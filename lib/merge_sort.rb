# frozen_string_literal: true

def merge_sort(n)
  return n if n.length == 1

  merge(merge_sort(n[0..n.length / 2 - 1]), merge_sort(n[n.length / 2..n.length - 1]))
end

def merge(a, b)
  index_a = 0
  until b.empty?
    if index_a == a.length
      a += b
      break
    end
    a[index_a] > b[0] ? a.insert(index_a, b.shift) : (index_a += 1)
  end
  a
end

puts merge_sort([5, 4]).to_s
puts merge_sort([5, 4, 3, 2, 1]).to_s
puts merge_sort([5, 4, 3, 2, 1, 6]).to_s

puts merge([1, 2], [3, 4]).to_s
puts merge([3, 4], [1, 2]).to_s
